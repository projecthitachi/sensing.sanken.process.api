﻿using Microsoft.AspNet.SignalR.Client;
using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Driver;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.ServiceProcess;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using uPLibrary.Networking.M2Mqtt;
using uPLibrary.Networking.M2Mqtt.Messages;

namespace sensing.sanken.process.api
{
    public partial class Service1 : ServiceBase
    {
        string appSettingsJsonPath = "";
        string appSettingsJson = "";
        JObject appSettingsJsonObj = new JObject();
        string mongodbConnectionString = "";
        string mongodbName = "";

        private MqttClient mqttClient = null;
        private string mqttPublishTopic = "";

        static HubConnection connection = null;
        static IHubProxy appHub = null;
        //static string hubUrl = "http://192.168.43.249:8090/signalr"; //local
        static string hubUrl = "http://192.168.1.100:8090/signalr"; //new-bcaserver
        //static string hubUrl = "http://10.0.1.39:8090/signalr"; //old-bcaserver

        public static MongoClient dbclient = new MongoClient("mongodb://192.168.1.100:27017"); //local-newbcasvr
        //public static MongoClient dbclient = new MongoClient("mongodb://10.0.1.198:27017"); //old-bcasvr

        public Service1()
        {
            InitializeComponent();
        }
        public void OnDebug()
        {
            OnStart(null);
        }
        protected override void OnStart(string[] args)
        {
            try
            {
                initRTHub();

                loadSettings();
                Thread t = new Thread(new ThreadStart(runService));
                t.Start();

                Thread s = new Thread(new ThreadStart(sensing));
                s.Start();

                Thread m = new Thread(new ThreadStart(sensinghourly));
                m.Start();
            }
            catch (Exception ex)
            {

            }
        }

        protected override void OnStop()
        {
        }
        private void loadSettings()
        {
            try
            {
                appSettingsJsonPath = AppDomain.CurrentDomain.BaseDirectory + "AppSettings.json";
                appSettingsJson = File.ReadAllText(appSettingsJsonPath);
                appSettingsJsonObj = JObject.Parse(appSettingsJson);

                mongodbConnectionString = appSettingsJsonObj["mongodbConnectionString"].ToString();
                mongodbName = appSettingsJsonObj["mongodbName"].ToString();

                //string mqttBroker = appSettingsJsonObj["mqttBroker"].ToString();
                string mqttBroker = "localhost";
                //string mqttSubscribeTopic = appSettingsJsonObj["mqttSubscribeTopic"].ToString();
                //mqttPublishTopic = appSettingsJsonObj["mqttPublishTopic"].ToString();
                string mqttSubscribeTopic = "API";
                mqttPublishTopic = "Datas";
                
                mqttClient = new MqttClient(mqttBroker);
                mqttClient.ProtocolVersion = MqttProtocolVersion.Version_3_1_1;
                mqttClient.MqttMsgPublishReceived += client_MqttMsgPublishReceived_1;
                mqttClient.Subscribe(new string[] { mqttSubscribeTopic + "#" }, new byte[] { MqttMsgBase.QOS_LEVEL_AT_MOST_ONCE });
                mqttClient.Connect("MQTT1");

            }
            catch(Exception ex)
            {

            }
        }

        private void client_MqttMsgPublishReceived_1(object sender, MqttMsgPublishEventArgs e)
        {

        }

        private void runService()
        {
            try
            {

                while (true)
                {
                    int apiInterval = 60000;
                    try
                    {
                        apiInterval = Convert.ToInt32(appSettingsJsonObj["apiInterval"].ToString());
                        string apiUsername = appSettingsJsonObj["apiUsername"].ToString();
                        string apiPassword = appSettingsJsonObj["apiPassword"].ToString();
                        string apiBaseUrl = appSettingsJsonObj["apiBaseUrl"].ToString();
                        string culture = appSettingsJsonObj["culture"].ToString();
                        JArray apiSettings = JArray.Parse(appSettingsJsonObj["apiSettings"].ToString());

                        List<AllObjectDataWhois> Result = new List<AllObjectDataWhois>();
                        var database = dbclient.GetDatabase("TOGO");
                        var collecCount = database.GetCollection<AllObjectDataWhois>("AllObjectDataWhois_Sensing_Services");
                        var collecRaw = database.GetCollection<SensingSankenRaw>("SensingSankenRaw");

                        var pipeline = collecCount.Find(f => f.Item != "" && f.DataType != "Derived").ToList();
                        double culture_ = 0.1;
                        if(culture == "false")
                        {
                            culture_ = 1.0;
                        }

                        // formula
                        AllObjectDataWhois Heat_Quantity_LHPS = collecCount.Find(f => f.Tag == "30150").FirstOrDefault(); // 30150
                        AllObjectDataWhois Heat_Quantity_DCU_1L = collecCount.Find(f => f.Tag == "30151").FirstOrDefault(); // 30151
                        AllObjectDataWhois Heat_Quantity_SHPS = collecCount.Find(f => f.Tag == "30152").FirstOrDefault(); // 30152
                        AllObjectDataWhois Power_Comsumption_2F = collecCount.Find(f => f.Tag == "30119").FirstOrDefault(); // 30119
                        AllObjectDataWhois PMV_1 = collecCount.Find(f => f.Tag == "30161").FirstOrDefault(); // 30161
                        AllObjectDataWhois PMV_2 = collecCount.Find(f => f.Tag == "30165").FirstOrDefault(); // 30165                        
                        AllObjectDataWhois PMV_3 = collecCount.Find(f => f.Tag == "30169").FirstOrDefault(); // 30169                        
                        AllObjectDataWhois PMV_4 = collecCount.Find(f => f.Tag == "30173").FirstOrDefault(); // 30173
                        AllObjectDataWhois CHWR_temp_s1 = collecCount.Find(f => f.Tag == "30078").FirstOrDefault(); // 30078
                        AllObjectDataWhois CHWR_temp_s2 = collecCount.Find(f => f.Tag == "30079").FirstOrDefault(); // 30079
                        AllObjectDataWhois CHWR_temp_s3 = collecCount.Find(f => f.Tag == "30080").FirstOrDefault(); // 30080
                        AllObjectDataWhois CHWR_temp_s4 = collecCount.Find(f => f.Tag == "30081").FirstOrDefault(); // 30081
                        AllObjectDataWhois CHW_flow_rate_s1 = collecCount.Find(f => f.Tag == "30025").FirstOrDefault(); // 30025
                        AllObjectDataWhois CHW_flow_rate_s2 = collecCount.Find(f => f.Tag == "30026").FirstOrDefault(); // 30026
                        AllObjectDataWhois CHW_flow_rate_s3 = collecCount.Find(f => f.Tag == "30027").FirstOrDefault(); // 30027
                        AllObjectDataWhois CHW_flow_rate_s4 = collecCount.Find(f => f.Tag == "30028").FirstOrDefault(); // 30028
                        AllObjectDataWhois Radiant_Panel_temp_z1 = collecCount.Find(f => f.Tag == "30087").FirstOrDefault(); // 30087
                        AllObjectDataWhois Radiant_Panel_temp_z2 = collecCount.Find(f => f.Tag == "30088").FirstOrDefault(); // 30088
                        AllObjectDataWhois Radiant_Panel_temp_z3 = collecCount.Find(f => f.Tag == "30089").FirstOrDefault(); // 30089
                        AllObjectDataWhois Radiant_Panel_temp_z4 = collecCount.Find(f => f.Tag == "30090").FirstOrDefault(); // 30090

                        // decimal parse
                        decimal CHWR_s1 = decimal.Parse(CHWR_temp_s1.PresentValue.Replace(".",","));
                        decimal CHWR_s2 = decimal.Parse(CHWR_temp_s2.PresentValue.Replace(".",","));
                        decimal CHWR_s3 = decimal.Parse(CHWR_temp_s3.PresentValue.Replace(".",","));
                        decimal CHWR_s4 = decimal.Parse(CHWR_temp_s4.PresentValue.Replace(".",","));
                        decimal CHW_s1 = decimal.Parse(CHW_flow_rate_s1.PresentValue.Replace(".",","));
                        decimal CHW_s2 = decimal.Parse(CHW_flow_rate_s2.PresentValue.Replace(".",","));
                        decimal CHW_s3 = decimal.Parse(CHW_flow_rate_s3.PresentValue.Replace(".",","));
                        decimal CHW_s4 = decimal.Parse(CHW_flow_rate_s4.PresentValue.Replace(".",","));
                        decimal Heat_LHPS = decimal.Parse(Heat_Quantity_LHPS.PresentValue.Replace(".",","));
                        decimal Heat_SHPS = decimal.Parse(Heat_Quantity_SHPS.PresentValue.Replace(".",","));
                        decimal Heat_DCU_1L = decimal.Parse(Heat_Quantity_DCU_1L.PresentValue.Replace(".",","));
                        decimal Power_2F = decimal.Parse(Power_Comsumption_2F.PresentValue.Replace(".",","));
                        decimal PMV_1_ = decimal.Parse(PMV_1.PresentValue.Replace(".",","));
                        decimal PMV_2_ = decimal.Parse(PMV_2.PresentValue.Replace(".",","));
                        decimal PMV_3_ = decimal.Parse(PMV_3.PresentValue.Replace(".",","));
                        decimal PMV_4_ = decimal.Parse(PMV_4.PresentValue.Replace(".",","));
                        decimal Radiant_temp_z1 = decimal.Parse(Radiant_Panel_temp_z1.PresentValue.Replace(".",","));
                        decimal Radiant_temp_z2 = decimal.Parse(Radiant_Panel_temp_z2.PresentValue.Replace(".",","));
                        decimal Radiant_temp_z3 = decimal.Parse(Radiant_Panel_temp_z3.PresentValue.Replace(".",","));
                        decimal Radiant_temp_z4 = decimal.Parse(Radiant_Panel_temp_z4.PresentValue.Replace(".",","));


                        // Cooling_Load_2F=(Heat_Quantity_SHPS + Heat_Quantity_LHPS) / 3.517
                        //Double Cooling_Load_2F = Convert.ToDouble(Heat_LHPS + Heat_SHPS) / 3.517 * (culture_);
                        Double Cooling_Load_2F = Convert.ToDouble(Heat_LHPS + Heat_SHPS) / 3.517 * (0.01);

                        // Cooling_Load_Density_2F=(Heat_Quantity_SHPS + Heat_Quantity_LHPS) *1000/185
                        //Double Cooling_Load_Density_2F = Convert.ToDouble(Heat_LHPS + Heat_SHPS) * 1000 / 185 * (culture_);
                        Double Cooling_Load_Density_2F = Convert.ToDouble(Heat_LHPS + Heat_SHPS) * 1000 / 185 * (0.01);

                        // Sanken_OSE_AD_2F=((Heat_Quantity_SHPS+Heat_Quantity_DCU-1L)/5.4+Power_Comsumption_2F)/(Cooling_Load_2F)
                        //Double Sanken_OSE_AD_2F = ((Convert.ToDouble(Heat_SHPS + Heat_DCU_1L) / 5.4 + Convert.ToDouble(Power_2F))) / (Cooling_Load_2F) * (culture_);
                        Double Sanken_OSE_AD_2F = ((Convert.ToDouble(Heat_SHPS + Heat_DCU_1L) / 5.4 + Convert.ToDouble(Power_2F))) / (Cooling_Load_2F) * (0.01);
                        if (Double.IsInfinity(Sanken_OSE_AD_2F) == true) { 
                            Sanken_OSE_AD_2F = 0;
                        }

                        //Double PMV = Convert.ToDouble(PMV_1_ + PMV_2_ + PMV_3_ + PMV_4_) / 4 * (culture_);
                        Double PMV = Convert.ToDouble(PMV_1_ + PMV_2_ + PMV_3_ + PMV_4_) / 4 * (0.01);

                        String AVG_CHWR_Temp = "18.0";
                        //Double AVG_CHWR_Temp = Convert.ToDouble(CHWR_s1 + CHWR_s2 + CHWR_s3 + CHWR_s4) / 4 * (culture_);
                        //Double AVG_CHW_Flow_Rate = Convert.ToDouble(CHW_s1 + CHW_s2 + CHW_s3  + CHW_s4) / 4 / 60 * (culture_);
                        Double AVG_CHW_Flow_Rate = Convert.ToDouble(CHW_s1 + CHW_s2 + CHW_s3) / 3 / 60 * (culture_);
                        Double AVG_Radiant_Panel = Convert.ToDouble(Radiant_temp_z1 + Radiant_temp_z2 + Radiant_temp_z3 + Radiant_temp_z4) / 4 * (culture_);

                        //Double AD_Energy_Consumption = Convert.ToDouble(Heat_SHPS + Heat_DCU_1L) / 5.4 + Convert.ToDouble(Power_2F) * (culture_);
                        Double AD_Energy_Consumption = Convert.ToDouble(Heat_SHPS + Heat_DCU_1L) / 5.4 + Convert.ToDouble(Power_2F) * (0.01);

                        //insert power consumption
                        var collectChart = database.GetCollection<SensingSankenWidgetChart>("SensingSankenWidgetChart");
                        SensingSankenWidgetChart vochart = new SensingSankenWidgetChart();
                        vochart.RecordTimestamp = DateTime.Now;
                        vochart.RecordStatus = 1;
                        vochart.FloorName = "Level 2";
                        vochart.Data = "Total Air Distribution System Energy Consumption";
                        vochart.Name = "AD_Energy_Consumption";
                        vochart.PresentValue = AD_Energy_Consumption.ToString();
                        collectChart.InsertOne(vochart);

                        // update present value
                        var BuilderFormula = Builders<AllObjectDataWhois>.Filter;

                        var filter_Cooling_Load_2F = BuilderFormula.Eq<string>("Name", "Cooling_Load_2F");
                        var filter_Cooling_Load_Density_2F = BuilderFormula.Eq<string>("Name", "Cooling_Load_Density_2F");
                        var filter_Sanken_OSE_AD_2F = BuilderFormula.Eq<string>("Name", "Sanken_OSE_AD_2F");
                        var filter_PMV = BuilderFormula.Eq<string>("Name", "AVG_PMV");
                        var filter_AVG_CHWR_Temp = BuilderFormula.Eq<string>("Name", "AVG_CHWR_Temp");
                        var filter_AVG_CHW_Flow_Rate = BuilderFormula.Eq<string>("Name", "AVG_CHW_Flow_Rate");
                        var filter_AVG_Radiant_Panel = BuilderFormula.Eq<string>("Name", "AVG_Radiant_Panel");

                        var update_Cooling_Load_2F = Builders<AllObjectDataWhois>.Update.Set("PresentValue", Math.Round(Cooling_Load_2F,1).ToString());
                        var update_Cooling_Load_Density_2F = Builders<AllObjectDataWhois>.Update.Set("PresentValue", Math.Round(Cooling_Load_Density_2F,1).ToString());
                        var update_Sanken_OSE_AD_2F = Builders<AllObjectDataWhois>.Update.Set("PresentValue", Math.Round(Sanken_OSE_AD_2F,1).ToString());
                        var update_PMV = Builders<AllObjectDataWhois>.Update.Set("PresentValue", Math.Round(PMV , 1).ToString());
                        var update_AVG_CHWR_Temp = Builders<AllObjectDataWhois>.Update.Set("PresentValue", AVG_CHWR_Temp.ToString());
                        var update_AVG_CHW_Flow_Rate = Builders<AllObjectDataWhois>.Update.Set("PresentValue", Math.Round(AVG_CHW_Flow_Rate, 1).ToString());
                        var update_AVG_Radiant_Panel = Builders<AllObjectDataWhois>.Update.Set("PresentValue", Math.Round(AVG_Radiant_Panel, 1).ToString());

                        collecCount.UpdateOne(filter_Cooling_Load_2F, update_Cooling_Load_2F);
                        collecCount.UpdateOne(filter_Cooling_Load_Density_2F, update_Cooling_Load_Density_2F);
                        collecCount.UpdateOne(filter_Sanken_OSE_AD_2F, update_Sanken_OSE_AD_2F);
                        collecCount.UpdateOne(filter_PMV, update_PMV);
                        collecCount.UpdateOne(filter_AVG_CHWR_Temp, update_AVG_CHWR_Temp);
                        collecCount.UpdateOne(filter_AVG_CHW_Flow_Rate, update_AVG_CHW_Flow_Rate);
                        collecCount.UpdateOne(filter_AVG_Radiant_Panel, update_AVG_Radiant_Panel);

                        send("WidgetRealtime", "SensortempData", "Cooling_Load_2F", Math.Round(Cooling_Load_2F , 1).ToString().Replace(",", "."));
                        send("WidgetRealtime", "SensortempData", "Cooling_Load_Density_2F", Math.Round(Cooling_Load_Density_2F , 1).ToString().Replace(",", "."));
                        send("WidgetRealtime", "SensortempData", "Sanken_OSE_AD_2F", Math.Round(Sanken_OSE_AD_2F , 1).ToString().Replace(",", "."));
                        send("WidgetRealtime", "SensortempData", "AVG_PMV", Math.Round(PMV , 1).ToString().Replace(",", "."));
                        send("WidgetRealtime", "SensortempData", "AVG_CHWR_Temp", AVG_CHWR_Temp.ToString().Replace(",","."));
                        send("WidgetRealtime", "SensortempData", "AVG_CHW_Flow_Rate", Math.Round(AVG_CHW_Flow_Rate , 1).ToString().Replace(",", "."));
                        send("WidgetRealtime", "SensortempData", "AVG_Radiant_Panel", Math.Round(AVG_Radiant_Panel , 1).ToString().Replace(",", "."));
                    }
                    catch (Exception ex)
                    {
                        
                    }
                    Thread.Sleep(apiInterval);
                }
            }
            catch (Exception ex)
            {

            }
        }

        private void sensinghourly()
        {
            try
            {

                while (true)
                {
                    int apiInterval = 300000;
                    try
                    {
                        int IntervalMinutes = DateTime.Now.Minute;
                        List<AllObjectDataWhois> Result = new List<AllObjectDataWhois>();
                        var database = dbclient.GetDatabase("TOGO");
                        var collecRaw = database.GetCollection<SensingSankenRaw>("SensingSankenRaw");
                        var collecHourly = database.GetCollection<SensingSankenHourly_REC>("SensingSankenHourly");

                        if (IntervalMinutes == 29 || IntervalMinutes == 30 || IntervalMinutes < 1)
                        {
                            var min = "00";
                            if (IntervalMinutes == 30){min = "30";}
                            if (IntervalMinutes == 29){min = "30";}
                            if (IntervalMinutes < 1){min = "00";}
                            DateTime half = DateTime.Now.AddMinutes(-30);
                            int Hour = half.Hour;
                            int Minutes = half.Minute;

                            var filter = new BsonDocument
                            {
                                {"$gte", DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd "+Hour+":"+Minutes+":dd"))},
                                {"$lte", DateTime.Now}
                            };

                            string match = "{$match: {RecordTimestamp : " + filter + " }}";
                            //string grp = "{$group:{_id:{RecordStatus:\"$RecordStatus\"},sa_flow_rate_l:{$sum:{$toDecimal:\"$sa_flow_rate_l\"}}}}";
                            //string grp = "{$group:{_id:{RecordStatus:\"$RecordStatus\"},sa_flow_rate_l:{$sum:{$toDecimal:\"$sa_flow_rate_l\"}}}}";
                            string grp_Heat_Quantity_SHPS = "{$group:{_id:{RecordStatus:\"$RecordStatus\"},Heat_Quantity_SHPS:{$sum:{$toDecimal:\"$Heat_Quantity_SHPS\"}}}}";
                            var pipeline_Heat_Quantity_SHPS = collecRaw.Aggregate().AppendStage<SensingSankenRaw>(match).AppendStage<SensingSankenHourly>(grp_Heat_Quantity_SHPS);
                            var res_Heat_Quantity_SHPS = pipeline_Heat_Quantity_SHPS.FirstOrDefault();

                            string grp_Heat_Quantity_DCU_1L = "{$group:{_id:{RecordStatus:\"$RecordStatus\"},Heat_Quantity_DCU_1L:{$sum:{$toDecimal:\"$Heat_Quantity_DCU_1L\"}}}}";
                            var pipeline_Heat_Quantity_DCU_1L = collecRaw.Aggregate().AppendStage<SensingSankenRaw>(match).AppendStage<SensingSankenHourly>(grp_Heat_Quantity_DCU_1L);
                            var res_Heat_Quantity_DCU_1L = pipeline_Heat_Quantity_DCU_1L.FirstOrDefault();

                            string grp_Power_Comsumption_2F = "{$group:{_id:{RecordStatus:\"$RecordStatus\"},Power_Comsumption_2F:{$sum:{$toDecimal:\"$Power_Comsumption_2F\"}}}}";
                            var pipeline_Power_Comsumption_2F = collecRaw.Aggregate().AppendStage<SensingSankenRaw>(match).AppendStage<SensingSankenHourly>(grp_Power_Comsumption_2F);
                            var res_Power_Comsumption_2F = pipeline_Power_Comsumption_2F.FirstOrDefault();

                            SensingSankenHourly_REC vohalf = new SensingSankenHourly_REC();
                            vohalf.RecordTimestamp = DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd HH:"+min+":00"));
                            vohalf.RecordStatus = 0;
                            vohalf.sa_flow_rate_l = 0;
                            vohalf.CHW_flow_rate_l = 0;
                            vohalf.CHW_flow_rate_s1 = 0;
                            vohalf.CHW_flow_rate_s2 = 0;
                            vohalf.CHW_flow_rate_s3 = 0;
                            vohalf.CHW_flow_rate_s4 = 0;
                            vohalf.sa_temp_l = 0;
                            vohalf.sa_rh_l = 0;
                            vohalf.CHWS_temp_l = 0;
                            vohalf.CHWR_temp_l = 0;
                            vohalf.CHWS_temp_s = 0;
                            vohalf.CHWR_temp_s1 = 0;
                            vohalf.CHWR_temp_s2 = 0;
                            vohalf.CHWR_temp_s3 = 0;
                            vohalf.CHWR_temp_s4 = 0;
                            vohalf.Radiant_Panel_temp_z1 = 0;
                            vohalf.Radiant_Panel_temp_z2 = 0;
                            vohalf.Radiant_Panel_temp_z3 = 0;
                            vohalf.Radiant_Panel_temp_z4 = 0;
                            vohalf.Power_Comsumption_2F = res_Power_Comsumption_2F.Power_Comsumption_2F / 30;
                            vohalf.Heat_Quantity_LHPS = 0;
                            vohalf.Heat_Quantity_DCU_1L = res_Heat_Quantity_DCU_1L.Heat_Quantity_DCU_1L / 30;
                            vohalf.Heat_Quantity_SHPS = res_Heat_Quantity_SHPS.Heat_Quantity_SHPS / 30;
                            vohalf.PMV_1 = 0;
                            vohalf.PMV_2 = 0;
                            vohalf.PMV_3 = 0;
                            vohalf.PMV_4 = 0;

                            if (IntervalMinutes == 30)
                            {
                                var bld = Builders<SensingSankenHourly_REC>.Filter;
                                var fltBld = bld.Eq<DateTime>("RecordTimestamp",DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd HH:"+30)));
                                var resbld = collecHourly.Find(fltBld).Count();

                                if (resbld > 0)
                                {
                                    collecHourly.InsertOne(vohalf);
                                }
                            }
                            else
                            {
                                collecHourly.InsertOne(vohalf);
                            }
                        }
                    }
                    catch (Exception ex)
                    {

                    }
                    Thread.Sleep(apiInterval);
                }
            }
            catch (Exception ex)
            {

            }
        }

        private void sensing()
        {
            try
            {

                while (true)
                {
                    int apiInterval = 60000;
                    try
                    {
                        apiInterval = Convert.ToInt32(appSettingsJsonObj["apiInterval"].ToString());
                        string apiUsername = appSettingsJsonObj["apiUsername"].ToString();
                        string apiPassword = appSettingsJsonObj["apiPassword"].ToString();
                        string allfloors = appSettingsJsonObj["allfloors"].ToString();
                        JArray apiSettings = JArray.Parse(appSettingsJsonObj["apiSettings"].ToString());

                        var database = dbclient.GetDatabase("TOGO");
                        var collecCount = database.GetCollection<SensingSankenAllFloor>("SensingSankenAllFloor");

                        string response = httpRequest(allfloors, apiUsername, apiPassword);
                        if (response != "")
                        {
                            // floor 2
                            JObject responseObj = JObject.Parse(response);
                            string records = responseObj["records"].ToString();
                            double sensing_ta = (double) Math.Round(Convert.ToDouble(responseObj["records"]["1"]["sensors"]["temperature"].ToString()),1); // sensing temperature
                            double sensing_rh = (double) Math.Round(Convert.ToDouble(responseObj["records"]["1"]["sensors"]["humidity"].ToString()),1); // sensing Humidity
                            double sensing_co2 = (double) Math.Round(Convert.ToDouble(responseObj["records"]["1"]["sensors"]["co2"].ToString()),1); // sensing c02
                            //var results = JsonConvert.DeserializeObject<List<Records>>(records);

                            // parse to signalR
                            send("WidgetRealtime", "SensortempData", "sensing_ta", sensing_ta.ToString().Replace(",", "."));
                            send("WidgetRealtime", "SensortempData", "sensing_rh", sensing_rh.ToString().Replace(",", "."));
                            send("WidgetRealtime", "SensortempData", "sensing_co2", sensing_co2.ToString().Replace(",", "."));

                            var BuilderFormula = Builders<SensingSankenAllFloor>.Filter;
                            var filter_all_Floor = BuilderFormula.Eq<string>("FloorName", "Level 2 Office");
                            var update_all_Floor = Builders<SensingSankenAllFloor>.Update.Set("sensing_ta", sensing_ta.ToString()).Set("sensing_rh", sensing_rh.ToString()).Set("sensing_co2", sensing_co2.ToString());
                            collecCount.UpdateOne(filter_all_Floor, update_all_Floor);
                        }
                    }
                    catch (Exception ex)
                    {

                    }
                    Thread.Sleep(apiInterval);
                }
            }
            catch (Exception ex)
            {

            }
        }

        private static void initRTHub()
        {
            try
            {
                //Set connection
                connection = new HubConnection(hubUrl);
                //Make proxy to hub based on hub name on server
                appHub = connection.CreateHubProxy("apphub");
                //Start connection
                connection.Start().ContinueWith(task =>
                {
                    if (task.IsFaulted)
                    {
                        task.Exception.GetBaseException();
                        Console.WriteLine(task.Exception.GetBaseException());
                    }
                    else
                    {
                        Console.WriteLine("Connected");
                    }
                }).Wait();
            }
            catch (Exception ex)
            {
                Console.WriteLine("ex init signalR " + ex.Message);
            }
        }
        private static void heandlerSignalR(string mod, string tp, string sp)
        {
            try
            {
                appHub.Invoke<string>("Send", mod, tp, sp).ContinueWith(task =>
                {
                    if (task.IsFaulted)
                    {
                        Console.WriteLine("There was an error calling send: {0}", task.Exception.GetBaseException());
                        //if (connection.State == ConnectionState.Disconnected)
                        //{
                        //    initRTHub();
                        //    heandlerSignalR(mod, tp, sp);
                        //}
                    }
                    else
                    {
                        Console.WriteLine("Result Handler signalR " + task.Result);
                    }
                });
            }
            catch (Exception ex)
            {
                Console.WriteLine("ex Handler signalR " + ex.Message);
                //if (connection.State == ConnectionState.Disconnected)
                //{
                //    initRTHub();
                //    heandlerSignalR(mod, tp, sp);
                //}
            }
        }
        private static void send( string client,string segment, string key,string data)
        {
            Dictionary<string, string> value = new Dictionary<string, string>();
            value.Add(key, data);
            string strValue = Convert.ToString(JsonConvert.SerializeObject(value));
            heandlerSignalR(client, segment, strValue);
        }
        private string httpRequest(string apiUrl, string apiUsername, string apiPassword)
        {
            string response = "";
            HttpClient httpClient = new HttpClient();
            string authenticationString = $"{apiUsername}:{apiPassword}";
            string base64EncodedAuthenticationString = Convert.ToBase64String(System.Text.ASCIIEncoding.ASCII.GetBytes(authenticationString));
            httpClient.DefaultRequestHeaders.Add("X-API-KEY", "R35TAP1#BM57107");
            HttpResponseMessage httpResponseMessage = httpClient.GetAsync(apiUrl).Result;
            if (httpResponseMessage.StatusCode == HttpStatusCode.OK)
            {
                response = httpResponseMessage.Content.ReadAsStringAsync().Result;
                if (TryParseJSON(response))
                {

                }
                else
                {
                    response = "";
                    //possible error
                }
            }
            else
            {
                Console.WriteLine(httpResponseMessage.Content.ReadAsStringAsync().Result);
                //possible error
            }
            return response;
        }

        private bool TryParseJSON(string json)
        {
            try
            {
                JObject.Parse(json);
                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}
