﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace sensing.sanken.process.api
{
    public class ApiResponses
    {
        public int errorcode { get; set; }
        public string title { get; set; }
        public string message { get; set; }
        public object data { get; set; }
        public ApiResponses()
        {
            errorcode = 0;
            title = "";
            message = "";
            data = new object();
        }
    }
    public class Records
    {
        public string tag { get; set; }
        public string mode { get; set; }
        public string airvel { get; set; }
        public string airflow { get; set; }
        public string coldwfdcu { get; set; }
        public string coldwfshps { get; set; }
        public string radpaneltemp { get; set; }
        public string westwindradtemp { get; set; }
        public string eastwindradtemp { get; set; }
        public string energysanken { get; set; }
        public string powersanken { get; set; }
        public string energypumpshps { get; set; }
        public string powerpumpshps { get; set; }
        public string energyfanlhps { get; set; }
        public string powerfanlhps { get; set; }
        public string energypaclhps { get; set; }
        public string powerpaclhps { get; set; }
        public string energyfcu { get; set; }
        public string powerfcu { get; set; }
        public string coolloadlhps { get; set; }
        public string coolloaddcu { get; set; }
        public string coolloadshps { get; set; }
        public string coolloadradpan { get; set; }
        public string coolloadfcu { get; set; }
        public string pmv { get; set; }
        public string mrt { get; set; }
        public string opertemp { get; set; }
        public string oaducttemp { get; set; }
        public string oaducthumid { get; set; }
        public string indoortemp { get; set; }
        public string indoorhumid { get; set; }
        public string saducttemp { get; set; }
        public string saducthumid { get; set; }
        public string coldsupplydcu { get; set; }
        public string coldretdcu { get; set; }
        public string coldsupplytemp { get; set; }
        public string coldsupplyshps { get; set; }
        public string coldrettemp { get; set; }
        public string coldretshps { get; set; }
        public string time { get; set; }
        public string location { get; set; }
        public string zone { get; set; }
    }
    public class ApiResults
    {
        public Records records { get; set; }
    }

    public class AllObjectDataWhois
    {
        [BsonId]
        public ObjectId _id { get; set; }
        [BsonDateTimeOptions(Kind = DateTimeKind.Local)]
        public DateTime RecordTimestamp { get; set; }
        public int RecordStatus { get; set; }
        public string FloorLevel { get; set; }
        public string Data { get; set; }
        public string Name { get; set; }
        public string Unit { get; set; }
        public string DataType { get; set; }
        public string Formula { get; set; }
        public string Tag { get; set; }
        public string Item { get; set; }
        public string PresentValue { get; set; }
    }

    public class SensingSankenAllFloor
    {
        [BsonId]
        public ObjectId _id { get; set; }
        [BsonDateTimeOptions(Kind = DateTimeKind.Local)]
        public DateTime RecordTimestamp { get; set; }
        public int RecordStatus { get; set; }
        public string FloorName { get; set; }
        public string sensing_light { get; set; }
        public string sensing_ta { get; set; }
        public string sensing_rh { get; set; }
        public string sensing_co2 { get; set; }
        public string sensing_voc { get; set; }
        public string sensing_noise { get; set; }
        public string sensing_pm25 { get; set; }
        public string sensing_pm10 { get; set; }
        public string sensing_pm01 { get; set; }
    }

    public class SensingSankenWidgetChart
    {
        [BsonId]
        public ObjectId _id { get; set; }
        [BsonDateTimeOptions(Kind = DateTimeKind.Local)]
        public DateTime RecordTimestamp { get; set; }
        public int RecordStatus { get; set; }
        public string FloorName { get; set; }
        public string Data { get; set; }
        public string Name { get; set; }
        public string PresentValue { get; set; }
    }

    public class SensingSankenRaw
    {
        [BsonId]
        public ObjectId _id { get; set; }
        [BsonDateTimeOptions(Kind = DateTimeKind.Local)]
        public DateTime RecordTimestamp { get; set; }
        public int RecordStatus { get; set; }
        public string sa_flow_rate_l { get; set; }
        public string CHW_flow_rate_l { get; set; }
        public string CHW_flow_rate_s1 { get; set; }
        public string CHW_flow_rate_s2 { get; set; }
        public string CHW_flow_rate_s3 { get; set; }
        public string CHW_flow_rate_s4 { get; set; }
        public string sa_temp_l { get; set; }
        public string sa_rh_l { get; set; }
        public string CHWS_temp_l { get; set; }
        public string CHWR_temp_l { get; set; }
        public string CHWS_temp_s { get; set; }
        public string CHWR_temp_s1 { get; set; }
        public string CHWR_temp_s2 { get; set; }
        public string CHWR_temp_s3 { get; set; }
        public string CHWR_temp_s4 { get; set; }
        public string Radiant_Panel_temp_z1 { get; set; }
        public string Radiant_Panel_temp_z2 { get; set; }
        public string Radiant_Panel_temp_z3 { get; set; }
        public string Radiant_Panel_temp_z4 { get; set; }
        public string Power_Comsumption_2F { get; set; }
        public string Heat_Quantity_LHPS { get; set; }
        public string Heat_Quantity_DCU_1L { get; set; }
        public string Heat_Quantity_SHPS { get; set; }
        public string PMV_1 { get; set; }
        public string PMV_2 { get; set; }
        public string PMV_3 { get; set; }
        public string PMV_4 { get; set; }
    }

    public class smart_id
    {
        public int RecordStatus { get; set; }
    }
    public class SensingSankenHourly
    {
        public smart_id _id { get; set; }
        public decimal sa_flow_rate_l { get; set; }
        public decimal CHW_flow_rate_l { get; set; }
        public decimal CHW_flow_rate_s1 { get; set; }
        public decimal CHW_flow_rate_s2 { get; set; }
        public decimal CHW_flow_rate_s3 { get; set; }
        public decimal CHW_flow_rate_s4 { get; set; }
        public decimal sa_temp_l { get; set; }
        public decimal sa_rh_l { get; set; }
        public decimal CHWS_temp_l { get; set; }
        public decimal CHWR_temp_l { get; set; }
        public decimal CHWS_temp_s { get; set; }
        public decimal CHWR_temp_s1 { get; set; }
        public decimal CHWR_temp_s2 { get; set; }
        public decimal CHWR_temp_s3 { get; set; }
        public decimal CHWR_temp_s4 { get; set; }
        public decimal Radiant_Panel_temp_z1 { get; set; }
        public decimal Radiant_Panel_temp_z2 { get; set; }
        public decimal Radiant_Panel_temp_z3 { get; set; }
        public decimal Radiant_Panel_temp_z4 { get; set; }
        public decimal Power_Comsumption_2F { get; set; }
        public decimal Heat_Quantity_LHPS { get; set; }
        public decimal Heat_Quantity_DCU_1L { get; set; }
        public decimal Heat_Quantity_SHPS { get; set; }
        public decimal PMV_1 { get; set; }
        public decimal PMV_2 { get; set; }
        public decimal PMV_3 { get; set; }
        public decimal PMV_4 { get; set; }
        public decimal Type { get; set; }
    }
    public class SensingSankenHourly_REC
    {
        [BsonId]
        public ObjectId _id { get; set; }
        [BsonDateTimeOptions(Kind = DateTimeKind.Local)]
        public DateTime RecordTimestamp { get; set; }
        public int RecordStatus { get; set; }
        public decimal sa_flow_rate_l { get; set; }
        public decimal CHW_flow_rate_l { get; set; }
        public decimal CHW_flow_rate_s1 { get; set; }
        public decimal CHW_flow_rate_s2 { get; set; }
        public decimal CHW_flow_rate_s3 { get; set; }
        public decimal CHW_flow_rate_s4 { get; set; }
        public decimal sa_temp_l { get; set; }
        public decimal sa_rh_l { get; set; }
        public decimal CHWS_temp_l { get; set; }
        public decimal CHWR_temp_l { get; set; }
        public decimal CHWS_temp_s { get; set; }
        public decimal CHWR_temp_s1 { get; set; }
        public decimal CHWR_temp_s2 { get; set; }
        public decimal CHWR_temp_s3 { get; set; }
        public decimal CHWR_temp_s4 { get; set; }
        public decimal Radiant_Panel_temp_z1 { get; set; }
        public decimal Radiant_Panel_temp_z2 { get; set; }
        public decimal Radiant_Panel_temp_z3 { get; set; }
        public decimal Radiant_Panel_temp_z4 { get; set; }
        public decimal Power_Comsumption_2F { get; set; }
        public decimal Heat_Quantity_LHPS { get; set; }
        public decimal Heat_Quantity_DCU_1L { get; set; }
        public decimal Heat_Quantity_SHPS { get; set; }
        public decimal PMV_1 { get; set; }
        public decimal PMV_2 { get; set; }
        public decimal PMV_3 { get; set; }
        public decimal PMV_4 { get; set; }
        public decimal Type { get; set; }
    }
}
